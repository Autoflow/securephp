<?php

// Version 2.0, 22.10.2016
# 1) get_error(), set_error() added
# 2) removed get_error_report(), removed get_exception()
# 3) changed to ErrorReports in case of error
# 4) extends USERCLASS
# 5) __invoke() added to get ftp stream, easily call $ftp()

// Update 08.08.2016
# 1) Rückgabewert von change_dir() korrigiert
# 2) neu: move()
# 3) neu: get_error_report() alias für get_exception()

// Version 1.0, 25.07.2016
# 1) Initial release, no info

/**
 * @package SECUREPHP
 * @author Alexander Münch
 * @copyright Alexander Münch
 * @version 22.10.2016
 */

namespace SECUREPHP

    {

    /**
     * Version of current ftp class.
     */

    define('SECUREPHP_FTP_VERSION', '2.0');

    /**
     * Class SAFEFTP
     * @package SECUREPHP
     */
    final class FTP extends USERCLASS

        {

        // FTP HEAD

        /**
         * Current connection resource.
         * @var resource
         */
        private $source;

        /**
         * @var \ErrorTicket
         */
        private $error;

        // FTP MAGIC METHODS

        /**
         * @return resource
         */
        public function __invoke()
            {
            return $this->source;
            }

        /**
         * Constructor.
         */
        public function __construct()
            {
            // NOTHING TO YET ...
            }

        // FTP METHODS

        /**
         * FTP-Verbindung herstellen.
         * @param $host
         * @param $user
         * @param null $pass
         * @param string $dir
         * @param null $port
         * @return bool
         */
        public function login($host, $user, $pass=NULL, $dir=".", $port=NULL)
            {
            if (is_null($host) || is_null($user))
                {
                $e = new \InitError('Verbindungsparameter dürfen nicht leer sein. Host- oder Benutzerangabe fehlt.');
                $e->add_param(['Host'], $host);
                $e->add_param(['Benutzer'], $user);
                $this->set_error($e);
                return false; // trigger_error('MYSQLI error. No login data provided.', E_USER_ERROR);
                }
            elseif(false == ($this->source = @ftp_connect($host, $port)))
                {
                $e = new \InitError('Keine Verbindung zum FTP-Server verfügbar. Die Verbindung ist nicht herstellbar.');
                $e->set_params(ARRAY('FTP-Server'=>$host, 'Port'=>$port));
                $this->set_error($e);
                return false;
                }
            elseif(false == @ftp_login($this->source, $user, $pass))
                {
                $e = new \InitError('Es ist kein Login am FTP-Server möglich.');
                $e->set_params(ARRAY('Host'=>$host, 'Port'=>$port, "Benutzer"=>$user, "Passwort"=>"***"));
                $this->set_error($e);
                return false;
                }
            elseif("." != $dir && false == $this->change_dir($dir))
                {
                return false;
                }
            else
                {
                return true;
                }
            }

        /**
         * @todo implement
         * Not in use yet ...
         * @param $uri
         * @return bool
         */
        public function login_by_uri($uri)
            {
            // Split FTP URI into:
            // $match[0] = ftp://username:password@sld.domain.tld/path1/path2/
            // $match[1] = ftp://
            // $match[2] = username
            // $match[3] = password
            // $match[4] = sld.domain.tld
            // $match[5] = /path1/path2/
            if(false === preg_match("/ftp:\/\/(.*?):(.*?)@(.*?)(\/.*)/i", $uri, $match))
                {
                $e = new \InitError('Keine Verbindung zum FTP-Server verfügbar. Die angebene URI ist fehlerhaft.');
                $e->set_params(ARRAY('URI' => $uri));
                $this->set_error($e);
                return false;
                }
            elseif(false == $this->login($match[1] . $match[4] . $match[5], $match[2], $match[3]))
                {
                return false;
                }
            elseif(false == ($dir = ftp_pwd($this->source)))
                {
                $e = new \InitError('Konnte das aktuelle FTP-Verzeichnis nicht ermitteln.');
                $e->set_params(ARRAY('URI' => $uri));
                $this->set_error($e);
                }
            elseif($dir != $match[5] && false == $this->change_dir($match[5]))
                {
                return false;
                }
            else
                {
                return true;
                }
            }

        /**
         * Change current directory.
         * @param $dir
         * @return bool
         */
        public function change_dir($dir)
            {
            if(false == @ftp_chdir($this->source, $dir))
                {
                $e = new \TransitionError('FTP-Verzeichniswechsel fehlgeschlagen.');
                $e->set_params(ARRAY('Aktuell' => ftp_pwd($this->source), 'Ziel'=>$dir));
                $this->set_error($e);
                return false;
                }
            else
                {
                return true;
                }
            }

        /**
         * @todo implement
         * @param null|string $directory
         * @return array
         */
        public function raw_list($directory=NULL)
            {
            if (is_array($children = @ftp_rawlist($this->source, $directory)))
                {
                $items = array();

                foreach ($children as $child)
                    {
                    $chunks = preg_split("/\s+/", $child);
                    list($item['rights'], $item['number'], $item['user'], $item['group'], $item['size'], $item['month'], $item['day'], $item['time']) = $chunks;
                    $item['type'] = $chunks[0]{0} === 'd' ? 'directory' : 'file';
                    array_splice($chunks, 0, 8);
                    $items[implode(" ", $chunks)] = $item;
                    }

                return $items;
                }
            }

        /**
         * Moves a file from A to B.
         * @param $old
         * @param $new
         * @return bool
         */
        public function move($old, $new)
            {
            if(false == @ftp_rename($this->source, $old, $new))
                {
                $e = new \TransitionError("Konnte Datei nicht von $old nach $new verschieben! ");
                $e->set_error_message((isset($php_errormsg)?$php_errormsg:''));
                $e->set_params(ARRAY('von'=>$old, 'nach'=>$new));
                $this->set_error($e);
                return false;
                }
            else
                {
                return true;
                }
            }

        // FTP GETTERS & SETTERS

        /**
         * @param \Exception $error
         */
        final protected function set_error(\Exception $error)
            {
            $this->error = $error;
            return true;
            }

        /**
         * @return \Exception
         */
        final public function get_error()
            {
            return $this->error;
            }
        }
    }

?>