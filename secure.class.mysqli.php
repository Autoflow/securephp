<?php

// Version 2.0, 22.10.2016
# 1) moved to \ErrorTickets for error reporting
# 2) added get_error(), set_error()
# 3) removed old error handlers

// Version 1.o
# initial release, no info

/**
* @package SECUREPHP
* @author Alexander Münch
* @copyright Alexander Münch
* @version 19.10.2016
*/

namespace SECUREPHP

    {

    /**
     * Version of current mysqli class.
     */
    define('SECUREPHP_MYSQL_VERSION', '2.0');

    /**
     * Class MYSQLI
     * @package SECUREPHP
     */
    final class MYSQLI extends \MYSQLI

        {

        /**
         * Implement basic Magic Methods
         * __set, __get, __call for protection.
         */
        use MAGIC;

        // MYSQLI HEAD

        /**
         * @var \ErrorTicket
         */
        private $user_error;

         // MYSQLI MAGIC METHODS

        /**
         * @return $this
         */
        public function __invoke()
            {
            return $this;
            }

        /**
         * Constructor.
         *
         * Adds error_report to MYSQLI class.
         *
         * @param null $host
         * @param null $user
         * @param null $pass
         * @param null $db
         * @param null $port
         * @param null $socket
         */
        public function __construct($host = NULL, $user = NULL, $pass = NULL, $db = NULL, $port = NULL, $socket = NULL)
            {
            /**
             * Disable auto login.
             */
            parent::__construct();
            }

        // MYSQLI METHODS

        /**
         * Connect to database.
         * @param $host
         * @param null|string $user
         * @param null|string $pass
         * @param null|string $db
         * @param null|int $port
         * @param null|string $socket
         * @return bool
         */
        public function login($host, $user = NULL, $pass = NULL, $db = NULL, $port = NULL, $socket = NULL)
            {

            if (is_null($host) || is_null($user))
                {
                $error = new \InitError('Verbindungsparameter dürfen nicht leer sein. Host- oder Benutzerangabe fehlt.');
                $error->add_param(['Host'], $host);
                $error->add_param(['Benutzer'], $user);
                $this->set_error($error);
                return false; // trigger_error('MYSQLI error. No login data provided.', E_USER_ERROR);
                }

            // INFO:
            // MYSQLI will raise warning-errors per default.
            // To prevent securephp from sending E_WARNING notifications switch to exceptions.
            // @ won't work. First, its a parent call, secondly, securephp does not recognize @ yet
            // example: E_WARNING: mysqli::connect(): (HY000/1045): Access denied for user 'test'@'localhost' (using password: YES)

            mysqli_report(MYSQLI_REPORT_ALL | MYSQLI_REPORT_STRICT);
            try
                {
                // Exceptions will not raise if database is not available!!
                // Therefore select db manually
                @parent::connect($host, $user, $pass, '', $port, $socket);
                }
            catch (\Exception $e)
                {
                $error = new \InitError('Es ist keine Verbindung zum Datenbankserver möglich.', NULL, $e);
                $error->set_description($e->getMessage());
                $params['host']      = $host;
                $params['user']      = $user;
                $params['db']        = $db;
                $params['port']      = $port;
                $params['socket']    = $socket;
                $error->set_params($params);
                $this->set_error($error);
                return false;
                }
            mysqli_report(MYSQLI_REPORT_ERROR);

            // trotzdem nochmal zur Sicherheit prüfen
            if ($this->connect_error)
                {
                $error = new \InitError('Es ist keine Verbindung zum Datenbankserver möglich.');
                $error->set_description($this->connect_error);
                $params['host']      = $host;
                $params['user']      = $user;
                $params['db']        = $db;
                $params['port']      = $port;
                $params['socket']    = $socket;
                $error->set_params($params);
                $this->set_error($error);
                return false;
                }
            elseif(false == $this->set_charset("utf8"))
                {
                $error = new \InitError('Es ist keine UTF8-Verbindung mit dem Datenbankserver möglich');
                $error->set_description($this->error);
                $this->set_error($error);
                return false;
                }

            // hier der Datenbankwechsel
            if(empty($db))
                {
                return true;
                }
            elseif(false == $this->select_db($db))
                {
                return false;
                }
            else
                {
                return true;
                }
            }

        /**
         * Change/select database.
         * @param string $db
         * @return bool
         */
        public function select_db($db)
            {
            if (false == parent::select_db($db))
                {
                $error = new \TransitionError('der Datenbankwechsel ist fehlgeschlagen');
                $error->add_param('Zieldatenbank', $db);
                $error->set_description($this->error);
                $this->set_error($error);
                return false;
                }
            else
                {
                return true;
                }
            }

        /**
         * Database query with exceptions.
         * @param string $query
         * @return bool|\mysqli_result|void
         */
        public function query($query)
            {
            // MYSQL_REPORT_ALL | ~MYSQLI_REPORT_INDEX
            mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
            try
                {
                $result = parent::query($query);
                }
            catch (\Exception $e)
                {
                $error = new \TransactionError('Die Datenbankabfrage ist fehlgeschlagen', NULL, $e);
                $error->set_description($e->getMessage());
                $params["query"] = $query;
                $error->set_params($params);
                $this->set_error($error);
                return false;
                }
            mysqli_report(MYSQLI_REPORT_ERROR);
            return $result;
            }

        // MYSQLI GETTERS & SETTERS

        /**
         * @param \ErrorTicket $e
         * @return bool
         */
        public function set_error(\ErrorTicket $e)
            {
            $this->user_error = $e;
            return true;
            }

        /**
         * @return \ErrorTicket
         */
        public function get_error()
            {
            return $this->user_error;
            }
        }
    }

?>